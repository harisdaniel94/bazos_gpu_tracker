import pandas as pd

from src.common.utils import load_json
from src.preprocessing.utils import mark_relevant_ad, preprocess_title, mark_low_memory_models
from src.scraper.bazos import BazosGPUScraper
from src.model.core import GPURecognizer


class GPUTrackerPipeline:

    def __init__(self, config: dict):
        self.config = config

    def scrape_new_ads(self) -> pd.DataFrame:
        scraper = BazosGPUScraper(self.config)
        scraper.scrape_all()
        scraper.save_ads()

        new_ads = scraper.load_new_ads()
        new_ads['title'] = new_ads['title'].apply(preprocess_title)
        new_ads = new_ads.loc[new_ads['title'].apply(mark_relevant_ad)]

        recognizer = GPURecognizer(self.config)
        new_ads['predicted_gpu'] = new_ads['title'].apply(recognizer.classify)

        new_ads = new_ads.loc[new_ads['predicted_gpu'] != 'other']
        new_ads = new_ads.loc[~new_ads['predicted_gpu'].apply(lambda m: mark_low_memory_models(m, 4))]
        new_ads.sort_values(by='predicted_gpu', inplace=True)
        new_ads.reset_index(drop=True, inplace=True)

        return new_ads


if __name__ == '__main__':
    config = load_json('config/config.json')
    pipeline = GPUTrackerPipeline(config)
    new_ads = pipeline.scrape_new_ads()
    new_ads.to_csv('data/predictions_new.csv', index=False, sep=';')
