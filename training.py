from src.common.utils import load_json
from src.model.core import GPURecognizerTraining


def run_training():
    config = load_json('config/config.json')
    gpu_training = GPURecognizerTraining(config)
    gpu_training.cross_validate()
    gpu_training.fit_model()
    gpu_training.save_model()
    gpu_training.evaluate_model()


if __name__ == '__main__':
    run_training()
