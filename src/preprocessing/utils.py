import re
import string

from unidecode import unidecode
from typing import List


def get_unique_terms(values: List[str]) -> List[str]:
    unique_terms = []
    for value in values:
        terms = [t.lower() for t in value.split()]
        for term in terms:
            if term and term not in unique_terms:
                unique_terms.append(term)
    return unique_terms


def drop_punctuations(s: str) -> str:
    return s.translate(str.maketrans(dict.fromkeys(string.punctuation, ' ')))


def replace_accents(s: str) -> str:
    return unidecode(s)


def separate_gpu_model_spec(ad: str) -> str:
    prep_ad = ad.lower()
    spec_match = re.search(r'(gtx|gt|rtx|rx|hd|o)\d+', prep_ad)
    if spec_match:
        prep_ad = prep_ad.replace(spec_match.group(1), f'{spec_match.group(1)} ')
    spec_match = re.search(r'\d+(ti|super|xt|fe|™)', prep_ad)
    if spec_match:
        prep_ad = prep_ad.replace(spec_match.group(1), f' {spec_match.group(1)}')
    prep_ad = re.sub(r'\s+', ' ', prep_ad)
    return prep_ad


def preprocess_ad(ad: str, gpu_terms: List[str] = None) -> str:
    prep_ad = ad.lower()
    prep_ad = replace_accents(prep_ad)
    prep_ad = drop_punctuations(prep_ad)
    prep_ad = separate_gpu_model_spec(prep_ad)
    if gpu_terms:
        filtered_words = list(filter(lambda w: w in gpu_terms, prep_ad.split()))
        prep_ad = ' '.join(get_unique_terms(filtered_words))
    return prep_ad


def preprocess_title(s: str) -> str:
    prep_s = s.lower()
    prep_s = replace_accents(prep_s)
    prep_s = drop_punctuations(prep_s)
    return prep_s


def drop_producer_name(gpu_model: str) -> str:
    return gpu_model.replace('amd radeon ', '').replace('nvidia geforce ', '')


def mark_relevant_ad(ad_title: str) -> bool:
    return all(word not in ad_title.lower() for word in ['kupim', 'predane', 'vymenim', 'vymena'])


def mark_low_memory_models(gpu_model: str, threshold: int = 4) -> bool:
    match = re.search(r'\d+(?=gb)', gpu_model.lower())
    if match:
        memory = int(match.group())
        return memory <= threshold
    return False
