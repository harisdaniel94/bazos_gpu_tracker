import pathlib
import numpy as np
import pandas as pd

from enum import Enum
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.model_selection import (
    train_test_split,
    cross_val_score,
    RepeatedStratifiedKFold)
from sklearn.ensemble import RandomForestClassifier

from src.common.utils import FileType, pickle_obj, load_obj, get_dir
from src.preprocessing.utils import preprocess_ad, mark_relevant_ad
from src.scraper.gpu_list import GPUList


class VectorizerType(Enum):
    count: str = 'count'
    tfidf: str = 'tfidf'


class GPURecognizerTraining:
    def __init__(self, config: dict):
        self.config = config['model']
        self.model_path = self.config['model_path']
        self.training_data_obj = self.TrainingData(config)
        self.training_data = self.training_data_obj.training_data
        self.gpu_list = self.training_data_obj.gpu_list
        self.features = self.training_data_obj.X
        self.labels = self.training_data_obj.y

    def get_plain_model(self) -> RandomForestClassifier:
        return RandomForestClassifier(n_estimators=100, class_weight='balanced', random_state=4)

    def cross_validate(self) -> None:
        X_train, X_test, y_train, y_test = train_test_split(
            self.features, self.labels, test_size=0.2, stratify=self.labels, random_state=4)
        scores = cross_val_score(
            self.get_plain_model(),
            X_train,
            y_train,
            scoring='accuracy',
            cv=RepeatedStratifiedKFold(n_splits=5, n_repeats=2, random_state=1), n_jobs=-1)
        cv_mean_score = np.mean(scores)
        cv_std_score = np.std(scores)
        print('> accuracy: %.3f +-(%.3f)' % (cv_mean_score, cv_std_score))

    def fit_model(self) -> None:
        self.model = self.get_plain_model()
        self.model.fit(self.features, self.labels)

    def save_model(self) -> None:
        pathlib.Path(get_dir(self.model_path)).mkdir(parents=True, exist_ok=True)
        pickle_obj(self.model, self.model_path)

    def evaluate_model(self):
        data = self.training_data.copy()
        df = pd.DataFrame({
            'original_ad': data['title'],
            'preprocessed_ad': data['title_prep'],
            'true_gpu_model': data['gpu_model_prep'],
            'pred_gpu_model': self.gpu_list.decode_values(self.model.predict(self.features))
        })
        df = df.loc[df['preprocessed_ad'].apply(mark_relevant_ad)]
        df.to_csv('data/predictions.csv', index=False, sep=';')
        correct_predictions = len(df[df['true_gpu_model'] == df['pred_gpu_model']])
        correct_predictions_rate = round((correct_predictions * 100) / len(df), 1)
        print(f'Correct predictions: {correct_predictions} / {len(df)} ({correct_predictions_rate}%)')

    class TrainingData:
        def __init__(self, config: dict):
            self.config = config
            self.model_config = config['model']
            self.training_data_path = self.model_config['training_data_path']
            self.vectorizer_type = self.model_config['vectorizer_type']
            self.vectorizer_max_ngram_range = self.model_config['vectorizer_max_ngram_range']
            self.gpu_model_encoder_path = self.model_config['gpu_model_encoder_path']
            self.ad_vectorizer_path = self.model_config['ad_vectorizer_path']

            self.load_gpu_list()
            self.load_training_data()
            self.preprocess_training_data()
            self.set_vectorizer()
            self.transform_training_data()
            self.save_objects()

        def load_training_data(self) -> None:
            if self.training_data_path.endswith(FileType.pickle.value):
                self.training_data = load_obj(self.training_data_path)
            elif self.training_data_path.endswith(FileType.csv.value):
                try:
                    self.training_data = pd.read_csv(self.training_data_path)
                except pd.errors.ParserError:
                    self.training_data = pd.read_csv(self.training_data_path, sep=';')
            elif self.training_data_path.endswith(FileType.excel.value):
                self.training_data = pd.read_excel(self.training_data_path)
            else:
                ValueError(
                    f'Not supported training data format {self.training_data_path!r}. Supporting: pkl, csv, xlsx.')

        def load_gpu_list(self) -> None:
            self.gpu_list = GPUList(self.config)
            self.gpu_list.encode_names()
            self.gpu_terms = self.gpu_list.get_unq_terms()

        def preprocess_training_data(self) -> None:
            self.training_data = self.training_data.loc[self.training_data['gpu_model'].notnull()]
            self.training_data['title_prep'] = self.training_data['title'].apply(lambda t: preprocess_ad(t))
            self.training_data['gpu_model_prep'] = self.training_data['gpu_model'].str.lower()

        def set_vectorizer(self) -> None:
            if self.vectorizer_type == VectorizerType.count.value:
                self.vectorizer = CountVectorizer(ngram_range=(1, self.vectorizer_max_ngram_range))
            elif self.vectorizer_type == VectorizerType.tfidf.value:
                self.vectorizer = TfidfVectorizer(ngram_range=(1, self.vectorizer_max_ngram_range))
            else:
                raise TypeError(f'Not supported vectorizer type {self.vectorizer_type}. Supporting count, tfidf.')

        def transform_training_data(self) -> None:
            vect_fit_base = self.training_data['title_prep'].tolist()
            vect_fit_base.extend(self.training_data['gpu_model_prep'].tolist())
            self.vectorizer.fit(vect_fit_base)
            self.X = self.vectorizer.transform(self.training_data['title_prep'])
            self.y = self.gpu_list.encode_values(self.training_data['gpu_model_prep'])

        def save_objects(self) -> None:
            pathlib.Path(get_dir(self.gpu_model_encoder_path)).mkdir(parents=True, exist_ok=True)
            pathlib.Path(get_dir(self.ad_vectorizer_path)).mkdir(parents=True, exist_ok=True)
            pickle_obj(self.gpu_list.name_encoder, self.gpu_model_encoder_path)
            pickle_obj(self.vectorizer, self.ad_vectorizer_path)


class GPURecognizer:
    def __init__(self, config: dict):
        self.config = config['model']
        self.model = load_obj(self.config['model_path'])
        self.encoder = load_obj(self.config['gpu_model_encoder_path'])
        self.vectorizer = load_obj(self.config['ad_vectorizer_path'])

    def classify(self, ad: str) -> str:
        prep_ad = preprocess_ad(ad)
        vec = self.vectorizer.transform([prep_ad])
        pred = self.model.predict(vec)
        return self.encoder.inverse_transform(pred)[0]
