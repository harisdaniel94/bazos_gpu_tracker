import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from src.common.utils import calc_mean, calc_median, calc_std
from src.preprocessing.utils import drop_producer_name


def show_values_on_bars(axs: plt.axes, h_v: str = "h", space: float = 1):
    def _show_on_single_plot(ax):
        if h_v == "v":
            for p in ax.patches:
                _x = p.get_x() + p.get_width() / 2
                _y = p.get_y() + p.get_height()
                value = int(p.get_height())
                ax.text(_x, _y, value, ha="center")
        elif h_v == "h":
            for p in ax.patches:
                _x = p.get_x() + p.get_width() + float(space)
                _y = p.get_y() + p.get_height() - 0.3
                value = int(p.get_width())
                ax.text(_x, _y, value, ha="left", fontsize=14)

    if isinstance(axs, np.ndarray):
        for idx, ax in np.ndenumerate(axs):
            _show_on_single_plot(ax)
    else:
        _show_on_single_plot(axs)


def plot_current_ads_histogram(df: pd.DataFrame, gpu_model_col: str, producer: str = None, save_path: str = None) -> None:
    plot_df = df.copy()
    plot_df['count'] = 1
    categories = plot_df.groupby([gpu_model_col], as_index=False).agg({'count': sum})
    if producer or producer != 'all':
        categories = categories.loc[categories[gpu_model_col].str.contains(producer, case=False)]
    categories[gpu_model_col] = categories[gpu_model_col].apply(lambda m: drop_producer_name(m).upper())
    categories.sort_values(by='count', inplace=True, ascending=False)

    if producer is None or producer == 'all':
        figsize = (10, 16)
    elif producer == 'amd':
        figsize = (8, 12)
    elif producer == 'nvidia':
        figsize = (10, 14)
    elif producer == 'mining':
        figsize = (8, 2)
    else:
        raise KeyError()
    f = plt.figure(figsize=figsize)
    g = sns.barplot(x="count", y=gpu_model_col, data=categories, label='count', color='goldenrod')
    show_values_on_bars(g, space=1.2)
    g.yaxis.set_label_position("right")
    g.yaxis.tick_right()
    g.invert_xaxis()
    g.set_ylabel('')
    g.set_xlabel('# relevant ads', fontsize=16)
    plt.yticks(fontsize=16)
    plt.tight_layout()
    if save_path:
        f.savefig(save_path)
    else:
        plt.show()


def prepare_df(df: pd.DataFrame, gpu_model: str, valid_range: tuple = (10, 3500)) -> pd.DataFrame:
    df_cpy = df.copy()
    df_cpy = df_cpy[df_cpy['predicted_gpu'] == gpu_model]
    df_cpy = df_cpy[df_cpy['price'].str.isdigit()]
    df_cpy['price'] = df_cpy['price'].astype(int)
    df_cpy = df_cpy[(df_cpy['price'] > min(valid_range)) & (df_cpy['price'] < max(valid_range))]
    df_cpy['predicted_gpu'] = df_cpy['predicted_gpu'].apply(lambda m: drop_producer_name(m).upper())
    return df_cpy


def plot_price_distribution(sold: pd.DataFrame, new: pd.DataFrame, gpu_model_name: str, hashrate: int, save_path: str):
    sold_cpy = prepare_df(sold, gpu_model_name)
    new_cpy = prepare_df(new, gpu_model_name)
    all_ads = pd.concat([sold_cpy, new_cpy])

    if len(all_ads) < 8:
        print(f'There is only {len(all_ads)} ads. Cannot show meaningful price analysis.')
        return

    mean, median, std = calc_mean(all_ads['price']), calc_median(all_ads['price']), calc_std(all_ads['price'])

    all_ads = all_ads[(all_ads['price'] > (mean - 3 * std)) & (all_ads['price'] < (mean + 3 * std))]

    mean, median, std = calc_mean(all_ads['price']), calc_median(all_ads['price']), calc_std(all_ads['price'])

    fig, ax1 = plt.subplots(figsize=(8, 2.5))
    sns.boxplot(x='price', y='predicted_gpu', data=all_ads, ax=ax1, color='#bbded6')
    sns.swarmplot(x='price', y='predicted_gpu', data=new_cpy, ax=ax1, color='dodgerblue', size=6)
    sns.swarmplot(x='price', y='predicted_gpu', data=sold_cpy, ax=ax1, color='indianred', size=5)

    ax1.yaxis.set_label_position('right')
    ax1.yaxis.tick_right()
    ax1.set_xlabel('€', fontsize=12)
    ax1.set_ylabel('')
    ax1.minorticks_on()
    ax1.axvline(median - std, color='#84c3b5')
    ax1.axvline(median + std, color='#84c3b5')
    ax1.tick_params(labelsize=14)

    if hashrate > 0:
        ax_t = ax1.secondary_xaxis('top')
        ax_t.tick_params(axis='x', direction='out')
        ax_t.minorticks_on()
        xticks = ax1.get_xticks().astype(np.int)
        xticks = np.round(xticks / hashrate, 1)
        ax_t.set_xticklabels(xticks)
        ax_t.tick_params(labelsize=14)
        ax_t.set_xlabel('€/MH', fontsize=12)

    plt.tight_layout()

    if save_path:
        fig.savefig(save_path)
    else:
        plt.show()
