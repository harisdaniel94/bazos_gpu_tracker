import time
import pathlib
import requests
import requests_random_user_agent
import pandas as pd

from itertools import product
from tqdm import tqdm
from bs4 import BeautifulSoup
from typing import List, Tuple, Dict
from sklearn.preprocessing import LabelEncoder

from src.common.utils import load_obj, pickle_obj
from src.preprocessing.utils import get_unique_terms


class GPUListDownloader:
    techpowerup_base_url: str = ('https://www.techpowerup.com/gpu-specs/?'
                                 'mfgr={producer}&released={year}&mobile=No&sort=name')
    techpowerup_columns: List[str] = ['name', 'producer', 'year']
    techpowerup_sleep_time: float = 6
    gpucheck_base_url: str = 'https://www.gpucheck.com/graphics-cards'
    gpucheck_columns: List[str] = ['name', 'producer', 'year', 'memory', 'orig_price_usd']

    def __init__(self, years: List[int], producers: List[str], source: str):
        self.years = years
        self.producers = producers
        self.source = source
        self.session = requests.Session()

    def download_techpowerup(self) -> List[Tuple[str, str, int]]:
        url = self.techpowerup_base_url
        gpu_list = []
        progress_bar = tqdm(product(self.years, self.producers))
        for year, producer in progress_bar:
            progress_bar.set_description(f"Downloading: {year}, {producer}")
            progress_bar.refresh()
            resp = self.session.get(url.format(producer=producer, year=year))
            s = BeautifulSoup(resp.text, 'html.parser')
            t = s.select('table.processors')[0]
            for td in t.select(f'td.vendor-{producer} > a'):
                gpu_list.append((td.text, producer, year))
            time.sleep(self.techpowerup_sleep_time)
        return gpu_list

    def download_gpucheck(self) -> List[Tuple[str, str, int, str, int]]:
        gpu_list = []
        resp = self.session.get(self.gpucheck_base_url)
        s = BeautifulSoup(resp.text, 'html.parser')
        progress_bar = tqdm(s.select('div.row')[1:])
        for row in progress_bar:
            name = row.select('div.col-6')[1].select('strong')[0].text
            progress_bar.set_description(f"Downloading: {name}")
            progress_bar.refresh()
            producer = name.split()[0]
            year = int(row.select('div.col-6')[1].select('span')[0].text)
            memory = row.select('div.col-6')[1].select('span')[1].text
            orig_price = row.select('div.col-6')[1].select('span')[2].text
            orig_price = orig_price.replace('$ ', '')
            orig_price = orig_price.replace(',', '')
            orig_price = orig_price.replace('.0', '')
            orig_price = int(orig_price)
            # if year not in years:
            #     continue
            if producer not in self.producers:
                continue
            if 'mobile' in name.lower() or 'max-q' in name.lower():
                continue
            gpu_list.append((name, producer, year, memory, orig_price))
        return gpu_list

    def execute(self) -> List[tuple]:
        if self.source == 'techpowerup':
            return self.download_techpowerup()
        elif self.source == 'gpucheck':
            return self.download_gpucheck()
        else:
            raise KeyError(f'{self.source!r} is not registered source.')


class GPUList:
    def __init__(self, config: dict):
        self.config = config['gpu_list']
        self.columns = self.config['columns']
        self.file_path = self.config['load_from_file']
        self.save_path = self.config['save_to_file']
        self.source = self.config['download_from_source']
        self.years = self.config['years']
        self.producers = self.config['producers']
        self.additional_classes = self.config['additional_classes']

        tmp = self.save_path.split('/')
        self.save_dir = '/'.join(tmp[:-1])
        self.save_file_name = tmp[-1]
        pathlib.Path(self.save_dir).mkdir(parents=True, exist_ok=True)

        if self.file_path:
            self.gpu_list = self.load_from_file()
        elif self.source:
            self.gpu_list = self.download()
        else:
            raise KeyError(f'Neither of load_from_file or source was specified to retrieve the gpu_list.')

        self.set_preprocessed()
        self.set_unq_name_list()
        self.encode_names()

    def load_from_file(self) -> List[tuple]:
        path = self.config['load_from_file']
        if path.endswith('.pkl'):
            return load_obj(path.endswith('.pkl'))
        elif path.endswith('.csv'):
            df = pd.read_csv(path, sep=';')
            return [tuple(x) for x in df.to_numpy()]
        elif path.endswith(('.xls', '.xlsx')):
            df = pd.read_excel(path)
            return [tuple(x) for x in df.to_numpy()]
        else:
            raise ValueError(f'Cannot load file {self.config["load_from_file"]!r}. Supporting pickle, CSV, XLS, XLSX.')

    def download(self) -> List[tuple]:
        if self.source == 'techpowerup':
            downloader = GPUListDownloader(self.years, self.producers, self.source)
            gpu_list = downloader.download_techpowerup()
        elif self.source == 'gpucheck':
            downloader = GPUListDownloader(None, None, self.source)
            gpu_list = downloader.download_gpucheck()
        else:
            raise ValueError(f'Cannot download gpu_list from source {self.source!r}. Supporting gpucheck, techpowerup.')
        return gpu_list

    def get_raw(self) -> List[tuple]:
        return self.gpu_list

    def set_preprocessed(self) -> None:
        gpu_list_prep = self.gpu_list
        gpu_list_prep = [(t[0].lower(),) + t[1:] for t in gpu_list_prep]
        self.prep_gpu_list = gpu_list_prep

    def get_preprocessed(self) -> List[tuple]:
        return self.prep_gpu_list

    def set_unq_name_list(self) -> None:
        self.unq_name_list = list(set(map(lambda t: t[0], self.prep_gpu_list)))
        self.unq_name_list.extend(self.additional_classes)

    def get_unq_terms(self) -> List[str]:
        return get_unique_terms(self.unq_name_list)

    def encode_names(self) -> None:
        self.name_encoder = LabelEncoder()
        self.name_encoder.fit(self.unq_name_list)
        self.encoded_names = self.name_encoder.transform(self.unq_name_list)
        self.encoding_map = dict(zip(self.encoded_names, self.name_encoder.classes_))

    def get_encoding_map(self) -> Dict[int, str]:
        return self.encoding_map

    def encode_values(self, values: List[str]) -> List[int]:
        return self.name_encoder.transform(values)

    def decode_values(self, values: List[int]) -> List[str]:
        return self.name_encoder.inverse_transform(values)

    def as_df(self) -> pd.DataFrame:
        return pd.DataFrame(self.gpu_list, columns=self.columns)

    def get_hashrate_map(self) -> Dict[str, int]:
        df = pd.DataFrame(self.prep_gpu_list)
        df.columns = self.columns
        df['hashrate'] = df['hashrate'].fillna(-1).astype(int)
        return df.set_index('name').to_dict()['hashrate']

    def save(self) -> None:
        if self.save_file_name.endswith('.pkl'):
            pickle_obj(self.gpu_list, self.save_path)
        elif self.save_file_name.endswith('.csv'):
            self.as_df().to_csv(self.save_path, index=False, sep=';')
        elif self.save_file_name.endswith(('.xls', '.xlsx')):
            self.as_df().to_excel(self.save_path, index=False)
        else:
            raise KeyError(f'Not supported file format {self.target_gpu_spec!r}. Supporting: pkl, csv, xlsx.')
