import re
import time
import pathlib
import requests
import requests_random_user_agent
import pandas as pd

from datetime import datetime
from typing import List, Tuple, Union
from bs4 import BeautifulSoup

from src.common.utils import load_latest_file, get_ads_path, save_file, FileType
from src.preprocessing.utils import preprocess_title


class BazosGPUScraper:
    base_url: str = 'https://pc.bazos.sk'
    dataframe_columns: List[str] = [
        'title',
        'date',
        'price',
        'city',
        'zipcode',
        'url',
        'img_path'
    ]

    def __init__(self, config: dict):
        self.config = config['bazos']
        self.wait_time = self.config['wait_time']
        self.images_dir = self.config['images_dir']
        self.ads_dir = self.config['ads_dir']
        self.ads_file_basename = self.config['ads_file_basename']
        self.ads_file_format = self.config['ads_file_format']
        self.ads_timestamp_format = self.config['ads_timestamp_format']
        self.sold_ads_dir = self.config['sold_ads_dir']
        self.new_ads_dir = self.config['new_ads_dir']
        self.first_url = f'{self.base_url}/graficka/'
        self.page_size = 20
        self.curr_page = 0
        self.number_of_ads = 0
        self.number_of_pages = 0
        pathlib.Path(self.ads_dir).mkdir(parents=True, exist_ok=True)
        pathlib.Path(self.images_dir).mkdir(parents=True, exist_ok=True)
        pathlib.Path(self.sold_ads_dir).mkdir(parents=True, exist_ok=True)
        self.session = requests.Session()

    @staticmethod
    def get_number_of_ads(text: str) -> int:
        number_of_ads = text.split('z')[-1]
        number_of_ads = number_of_ads.replace(' ', '')
        return int(number_of_ads)

    def get_number_of_pages(self) -> int:
        number_of_pages = self.number_of_ads // self.page_size
        if number_of_pages % self.page_size != 0:
            number_of_pages += 1
        return number_of_pages

    @staticmethod
    def save_ad_image(url: str, path: str) -> None:
        resp = requests.get(url)
        with open(path, 'wb') as f:
            f.write(resp.content)

    def get_ad_image_path(self, img_url: str) -> str:
        if self.base_url.endswith('sk'):
            return img_url.replace('https://www.bazos.sk/', '').split('?')[0].replace('/', '_')
        elif self.base_url.endswith('cz'):
            return img_url.replace('https://www.bazos.cz/', '').split('?')[0].replace('/', '_')
        else:
            raise ValueError('Not registered bazos locale.')

    @staticmethod
    def extract_date(text: str) -> Union[str, None]:
        try:
            return re.search(r'(?<=\[).*?(?=\])', text).group().replace(' ', '')
        except AttributeError:
            return None

    @staticmethod
    def extract_price(text: str) -> Union[str, int]:
        try:
            return int(re.search(r'\d+([\.\,]\d+)?', text.replace(' ', '')).group())
        except (AttributeError, ValueError):
            return text.strip()

    def get_next_urls(self, urls: List[str]) -> Tuple[str, str]:
        next_to_last_url = f'{self.base_url}{urls[-2]}'
        last_url = f'{self.base_url}{urls[-1]}'
        if next_to_last_url != last_url:
            next_url = last_url
            last_url = next_to_last_url
        else:
            next_url = last_url
            last_url = None
        return next_url, last_url

    def scrape_page(self, url: str) -> Tuple[List[Tuple[str, str, str, str, str, str, str]], str, str]:
        page_ads = []
        
        resp = self.session.get(url)
        s = BeautifulSoup(resp.text, 'html.parser')
        urls = [x.get('href') for x in s.select('div.strankovani > a')]
        next_url, last_url = self.get_next_urls(urls)
        
        if not self.number_of_ads:
            self.number_of_ads = self.get_number_of_ads(s.select('div.listainzerat')[0].find_all('div')[0].text)
            self.number_of_pages = self.get_number_of_pages()
            self.number_of_ads_len = len(str(self.number_of_ads))

        ads = s.find_all('div', class_='inzeraty')
        for i, ad in enumerate(ads):
            date = self.extract_date(ad.select('span.velikost10')[0].text)
            title = ad.select('span.nadpis')[0].text
            title = re.sub(r'\s+', ' ', title)
            price = self.extract_price(ad.select('div.inzeratycena')[0].text)
            # location = ads[0].find_all("td", {"valign": "top"})[1].get_text(separator="\n").split('\n')
            location = ad.select('div.inzeratylok')[0].get_text(separator='\n').split('\n')
            city = location[0]
            zipcode = location[1].replace(' ', '')
            ad_url = f"{self.base_url}/{ad.select('span.nadpis')[0].select('a')[0].get('href')[1:]}"
            try:
                ad_image_url = ad.select('img')[0].get('src')
                img_path = f'{self.images_dir}/{self.get_ad_image_path(ad_image_url)}'
                self.save_ad_image(ad_image_url, img_path)
            except IndexError:
                img_path = None
            page_ads.append((title, date, price, city, zipcode, ad_url, img_path))
            curr_ad_downloaded = str((i + 1) + (self.curr_page * self.page_size)).zfill(self.number_of_ads_len)
            print(f'> {curr_ad_downloaded}/{self.number_of_ads} ads downloaded', end='\r')

        return page_ads, next_url, last_url

    def scrape_all(self) -> None:
        self.ads = []
        ads, next_url, last_url = self.scrape_page(self.first_url)
        while last_url is not None:
            time.sleep(self.wait_time)
            self.curr_page += 1
            new_ads, next_url, last_url = self.scrape_page(next_url)
            self.ads.extend(new_ads)
        else:
            new_ads, _, _ = self.scrape_page(next_url)
            self.ads.extend(new_ads)
        print(f'> Downloaded {len(self.ads)} ads through {self.number_of_pages} pages.')

    def get_ads_as_df(self) -> pd.DataFrame:
        return pd.DataFrame(self.ads, columns=self.dataframe_columns)

    def save_marked_as_sold_ads(self, ts: str) -> None:
        sold_ads = self.get_ads_as_df()
        sold_ads['title_prep'] = sold_ads['title'].apply(preprocess_title)
        sold_ads = sold_ads.loc[sold_ads['title_prep'].apply(self.AdSelector.mark_as_sold)]
        sold_ads.drop('title_prep', axis=1, inplace=True)
        sold_path = f'{self.sold_ads_dir}/sold_ads_{ts}.csv'
        sold_ads.to_csv(sold_path, index=False, sep=';')

    def save_ads(self) -> None:
        ts = datetime.now().strftime(self.ads_timestamp_format)
        path = f'{self.ads_dir}/{self.ads_file_basename}_{ts}.{self.ads_file_format}'
        if self.ads_file_format == FileType.pickle.value:
            save_file(path, self.ads)
        else:
            save_file(path, self.get_ads_as_df())
        self.save_marked_as_sold_ads(ts)
        ad_selector = self.AdSelector(self.config, ts)
        ad_selector.save_new_sold_ads()
        ad_selector.save_new_ads()
        ad_selector.remove_new_sold_ads()

    def load_new_ads(self) -> pd.DataFrame:
        return load_latest_file(self.new_ads_dir, 0)

    class AdSelector:
        def __init__(self, config: dict, ts: str):
            self.ads_dir = config['ads_dir']
            self.sold_ads_dir = config['sold_ads_dir']
            self.new_ads_dir = config['new_ads_dir']
            pathlib.Path(self.new_ads_dir).mkdir(parents=True, exist_ok=True)
            self.latest_ads = load_latest_file(self.ads_dir, 0)
            self.latest_sold_ads = load_latest_file(self.sold_ads_dir, 0)
            self.prev_ads = load_latest_file(self.ads_dir, 1)
            self.prev_sold_ads = pd.read_csv(f'{self.sold_ads_dir}/sold.csv', sep=';')
            self.ts = ts
            # need to filter relevant ads (use 'mark_relevant_ad' from preprocessing/utils)
            # need to filter ads classified as 'other' by the model, however it should be done outside of this class

        @staticmethod
        def mark_as_sold(s: str) -> bool:
            return re.search(r'pr[eo]dan[aey]', s) is not None

        def get_marked_as_sold_ad_urls(self, ads: pd.DataFrame) -> List[str]:
            return ads.loc[ads['title'].apply(lambda t: self.mark_as_sold(preprocess_title(t))), 'url'].tolist()

        def get_new_marked_as_sold_ads(self) -> pd.DataFrame:
            prev_marked_as_sold_urls = self.get_marked_as_sold_ad_urls(self.prev_sold_ads)
            curr_marked_as_sold_urls = self.get_marked_as_sold_ad_urls(self.latest_sold_ads)
            new_marked_as_sold_urls = list(set(curr_marked_as_sold_urls) - set(prev_marked_as_sold_urls))
            return self.latest_sold_ads.loc[self.latest_sold_ads['url'].isin(new_marked_as_sold_urls)]

        def get_sold_ad_urls(self) -> List[str]:
            return list(set(self.prev_ads['url']) - set(self.latest_ads['url']))

        def get_new_sold_ads(self) -> pd.DataFrame:
            return self.prev_ads.loc[self.prev_ads['url'].isin(self.get_sold_ad_urls())]

        def get_new_ad_urls(self) -> List[str]:
            return list(set(self.latest_ads['url']) - set(self.prev_ads['url']))

        def get_new_ads(self) -> pd.DataFrame:
            return self.latest_ads.loc[self.latest_ads['url'].isin(self.get_new_ad_urls())]

        def save_new_sold_ads(self) -> None:
            new_sold_ads = pd.concat([self.get_new_marked_as_sold_ads(), self.get_new_sold_ads()])
            new_sold_ads['ts'] = self.ts
            self.prev_sold_ads = pd.concat([new_sold_ads, self.prev_sold_ads])
            save_file(f'{self.sold_ads_dir}/sold.csv', self.prev_sold_ads)

        def save_new_ads(self) -> None:
            new_ads = self.get_new_ads()
            save_file(f'{self.new_ads_dir}/new_ads_{self.ts}.csv', new_ads)

        def remove_new_sold_ads(self):
            pathlib.Path(f'{self.sold_ads_dir}/sold_ads_{self.ts}.csv').unlink()
