import re
import json
import pickle
import pathlib
import numpy as np
import pandas as pd

from enum import Enum
from datetime import datetime, timedelta
from typing import List, Dict, Union


class FileType(Enum):
    pickle: str = 'pkl'
    csv: str = 'csv'
    excel: str = ('xls', 'xlsx')


def load_json(path: str) -> Dict[str, int]:
    with open(path, 'r') as fr:
        return json.loads(fr.read())


def load_obj(path: str) -> List[tuple]:
    with open(path, 'rb') as fr:
        return pickle.load(fr)


def pickle_obj(obj: object, path: str) -> None:
    with open(path, 'wb') as fw:
        pickle.dump(obj, fw)


def get_dir(path: str) -> str:
    p = pathlib.Path(path)
    return str(p.parents[0])


def get_ads_path(folder: str, idx: int) -> str:
    folder_path = pathlib.Path(folder)
    all_paths = [p.name for p in folder_path.iterdir() if not p.name.startswith('.') and not p.is_dir()]
    all_paths = sorted(all_paths, reverse=True)
    return f'{folder}/{all_paths[idx]}'


def get_date_from_path(path: str) -> datetime.date:
    m = re.search(r'\d+', path).group()
    return datetime.strptime(m, '%Y%m%d')


def get_all_paths_by_date(folder: str, days: int = 0) -> list:
    last_date = (datetime.now() - timedelta(days=days+1))
    return sorted([
        path.name
        for path in [p for p in pathlib.Path(folder).iterdir() if not p.name.startswith('.') and not p.is_dir()]
        if not path.name.startswith('.') and not path.is_dir() and last_date <= get_date_from_path(path.name)
    ])


def load_latest_file(folder: str, idx: int = 0) -> pd.DataFrame:
    path = get_ads_path(folder, idx)
    print(path)
    if path.endswith('pkl'):
        return pd.DataFrame(load_obj(path))
    elif path.endswith('csv'):
        try:
            return pd.read_csv(path, sep=';')
        except pd.errors.ParserError:
            return pd.read_csv(path, sep=',')
    elif path.endswith(('xlsx', 'xls')):
        return pd.read_excel(path)
    else:
        raise KeyError(f'Not supported file format {path!r}. Supporting: pkl, csv, xlsx.')


def save_file(path: str, obj: Union[list, pd.DataFrame]) -> None:
    if path.endswith(FileType.pickle.value):
        pickle_obj(obj, path)
    elif path.endswith(FileType.csv.value):
        obj.to_csv(path, index=False, sep=';')
    elif path.endswith(FileType.excel):
        obj.to_excel(path, index=False)
    else:
        raise KeyError(f'Not supported file format {path!r}. Supporting: pkl, csv, xlsx.')


def calc_mean(values: pd.Series) -> int:
    return round(values.mean())


def calc_median(values: pd.Series) -> int:
    return round(values.median())


def calc_std(values: pd.Series) -> int:
    return round(values.std())
