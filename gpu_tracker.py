import logging
import pandas as pd
import pathlib

from os.path import basename
from time import sleep
from typing import Dict
from telegram import Update
from telegram.ext import Updater, CommandHandler, CallbackContext


from pipeline import GPUTrackerPipeline
from src.common.utils import load_json, load_latest_file, get_ads_path
from src.scraper.gpu_list import GPUList
from src.preprocessing.utils import preprocess_title, mark_relevant_ad, mark_low_memory_models, drop_producer_name
from src.model.core import GPURecognizer
from src.visualization.plots import plot_current_ads_histogram, plot_price_distribution

# Enable logging
logging.basicConfig(
    format='%(asctime)s - %(message)s', level=logging.INFO
)
logger = logging.getLogger(__name__)

# global variables
CONFIG = load_json('config/config.json')
PIPELINE = GPUTrackerPipeline(CONFIG)
GPU_LIST = GPUList(CONFIG)
RECOGNIZER = GPURecognizer(CONFIG)

pathlib.Path(CONFIG['bazos']['plots_dir']).mkdir(parents=True, exist_ok=True)


def remove_job_if_exists(name, context):
    """Remove job with given name. Returns whether job was removed."""
    current_jobs = context.job_queue.get_jobs_by_name(name)
    if not current_jobs:
        return False
    for job in current_jobs:
        job.schedule_removal()
    return True


def help(update: Update, context: CallbackContext) -> None:
    help_msg_lines = [
        'GPU Tracker commands:',
        '/help',
        '/run_once',
        '/run_repeating <minutes>',
        '/stop',
        '/hist {amd, nvidia, all}',
        '/hashrates',
        '/price "gpu model name"'
    ]
    update.message.reply_text('\n'.join(help_msg_lines))


def get_price_per_mh(row: pd.Series, hashrate_map: Dict[str, int]) -> float:
    try:
        price = int(row['price'])
        hashrate = hashrate_map[row['predicted_gpu']]
        return float('inf') if hashrate < 0 else round((price / hashrate), 1)
    except:
        return float('inf')


def track_gpus(context) -> None:
    global CONFIG
    global PIPELINE
    global GPU_LIST

    job = context.job
    logger.info("GPU Tracking initiated.")
    # retrieve new ads
    # config = load_json('config/config.json')
    # pipeline = GPUTrackerPipeline(config)
    hashrate_map = GPU_LIST.get_hashrate_map()
    new_ads = PIPELINE.scrape_new_ads()
    new_ads['price_per_mh'] = new_ads.apply(lambda row: get_price_per_mh(row, hashrate_map), axis=1)
    new_ads.sort_values(by='price_per_mh', ascending=True, inplace=True)
    new_ads.reset_index(drop=True, inplace=True)
    logger.info("GPU Tracking finished. Sending the results.")
    context.bot.send_message(job.context, text=f'Found {len(new_ads)} relevant ads.')
    sleep(3)
    for idx, row in new_ads.iterrows():
        model_name = row["predicted_gpu"].upper()
        model_name = model_name.replace('NVIDIA GEFORCE ', '').replace('AMD RADEON ', '')
        text = f'{idx + 1}/{len(new_ads)} {model_name}\n'
        if row['price_per_mh'] != float('inf'):
            text += f'Price per MH: {row["price_per_mh"]} €\n'
        text += f'{row["url"]}'
        context.bot.send_message(job.context, text=text)
        sleep(1)


def run_once(update: Update, context: CallbackContext) -> None:
    try:
        chat_id = update.message.chat_id
        # remove scraping job if already exist
        remove_job_if_exists(str(chat_id), context)
        context.job_queue.run_once(track_gpus, when=5, context=chat_id, name=str(chat_id))
        update.message.reply_text(f'GPU Tracker engaged. Once.')
    except (IndexError, ValueError):
        update.message.reply_text('Usage: /run-once')


def run_repeating(update: Update, context: CallbackContext) -> None:
    try:
        chat_id = update.message.chat_id
        time_interval_m = int(context.args[0]) * 60
        # remove scraping job if already exist
        remove_job_if_exists(str(chat_id), context)
        context.job_queue.run_repeating(track_gpus, time_interval_m, first=10, context=chat_id, name=str(chat_id))
        update.message.reply_text(f'GPU Tracker engaged. Every {int(context.args[0])} minutes.')
    except (IndexError, ValueError):
        update.message.reply_text('Usage: /run-repeating <minutes>')


def stop(update: Update, context: CallbackContext) -> None:
    chat_id = update.message.chat_id
    job_removed = remove_job_if_exists(str(chat_id), context)
    text = 'GPU Tracker halted.' if job_removed else 'GPU Tracker not running.'
    update.message.reply_text(text)


def show_hashrates(update: Update, context: CallbackContext) -> None:
    global GPU_LIST
    hashrate_map = GPU_LIST.get_hashrate_map()
    text = ""
    for gpu_name, hashrate in sorted(hashrate_map.items(), key=lambda t: t[0]):
        gpu_name_short = drop_producer_name(gpu_name.lower()).upper()
        text += f'{gpu_name_short}: <b>{hashrate if hashrate > 0 else "NA"}</b>\n'
    context.bot.send_message(update.message.chat_id, text, parse_mode='HTML')


def save_current_ads_histogram(producer: str = None) -> str:
    global CONFIG
    global RECOGNIZER
    path = get_ads_path(CONFIG['bazos']['ads_dir'], 0)
    df = load_latest_file(CONFIG['bazos']['ads_dir'], 0)
    df['title'] = df['title'].apply(preprocess_title)
    df = df.loc[df['title'].apply(mark_relevant_ad)]
    df['predicted_gpu'] = df['title'].apply(RECOGNIZER.classify)
    df = df.loc[df['predicted_gpu'] != 'other']
    df = df.loc[~df['predicted_gpu'].apply(lambda m: mark_low_memory_models(m, 4))]
    save_path = f'{CONFIG["bazos"]["plots_dir"]}/{basename(path).split(".")[0]}_hist.png'
    plot_current_ads_histogram(df, 'predicted_gpu', producer, save_path)
    return save_path


def show_current_ads_histogram(update: Update, context: CallbackContext) -> None:
    try:
        update.message.reply_text('Generating histogram of current ads. Please wait.')
        producer = context.args[0]
        if producer not in ['amd', 'nvidia', 'all']:
            raise KeyError()
        logger.info('Working on histogram of current GPU model ads')
        plot_path = save_current_ads_histogram(producer)
        img = open(plot_path, 'rb')
        context.bot.send_photo(update.message.chat_id, photo=img, caption='Histogram of current GPU model ads')
    except:
        update.message.reply_text('Usage: /hist {amd, nvidia, all}')


def save_price_analysis_plot(gpu_model: str) -> str:
    global CONFIG
    global GPU_LIST
    global RECOGNIZER

    sold = load_latest_file(CONFIG['bazos']['sold_ads_dir'], 0)
    sold['title'] = sold['title'].apply(preprocess_title)
    sold = sold.loc[sold['title'].apply(mark_relevant_ad)]
    sold['predicted_gpu'] = sold['title'].apply(RECOGNIZER.classify)
    sold = sold.loc[sold['predicted_gpu'] != 'other']

    new = load_latest_file(CONFIG['bazos']['ads_dir'], 0)
    new['title'] = new['title'].apply(preprocess_title)
    new = new.loc[new['title'].apply(mark_relevant_ad)]
    new['predicted_gpu'] = new['title'].apply(RECOGNIZER.classify)
    new = new.loc[new['predicted_gpu'] != 'other']

    hashrate_map = GPU_LIST.get_hashrate_map()
    hashrate = hashrate_map[gpu_model]

    save_path = f'{CONFIG["bazos"]["plots_dir"]}/price_analysis_{gpu_model.lower().replace(" ", "_")}.png'
    plot_price_distribution(sold, new, gpu_model, hashrate, save_path)
    return save_path


def show_price_analysis(update: Update, context: CallbackContext) -> None:
    global RECOGNIZER
    try:
        gpu_model = ' '.join(context.args)
        gpu_model = RECOGNIZER.classify(gpu_model)
        if gpu_model == 'other':
            update.message.reply_text(f"Couldn't recognize model {gpu_model}. Try a different GPU model.")
            return
        update.message.reply_text('Retrieving price analysis. Please wait.')
        plot_path = save_price_analysis_plot(gpu_model)
        img = open(plot_path, 'rb')
        gpu_model_short = drop_producer_name(gpu_model.lower()).upper()
        context.bot.send_photo(update.message.chat_id, photo=img, caption=f'Price analysis of {gpu_model_short}')
    except:
        update.message.reply_text('Usage: /price "gpu model name"')


def main():
    global CONFIG
    """Run bot."""

    # Create the Updater and pass it your bot's token.
    updater = Updater(CONFIG['telegram']['token'], use_context=True)

    # Get the dispatcher to register handlers
    dispatcher = updater.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler('help', help))
    dispatcher.add_handler(CommandHandler('run_once', run_once))
    dispatcher.add_handler(CommandHandler('run_repeating', run_repeating))
    dispatcher.add_handler(CommandHandler('stop', stop))
    dispatcher.add_handler(CommandHandler('hist', show_current_ads_histogram))
    dispatcher.add_handler(CommandHandler('hashrates', show_hashrates))
    dispatcher.add_handler(CommandHandler('price', show_price_analysis))

    # Start the Bot
    updater.start_polling()

    # Block until you press Ctrl-C or the process receives SIGINT, SIGTERM or
    # SIGABRT. This should be used most of the time, since start_polling() is
    # non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
